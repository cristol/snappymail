#!/bin/sh

function LOG {
  if [ $1 == "INFO" ]; then
    echo -e "$(date +%Y-%m-%d' '%H:%M:%S)\e[32m $1 $2 \e[39m"
  elif [ $1 == "WARN" ]; then
    echo -e "$(date +%Y-%m-%d' '%H:%M:%S)\e[33m $1 $2 \e[39m"
  elif [ $1 == "ERROR" ]; then
    echo -e "$(date +%Y-%m-%d' '%H:%M:%S)\e[31m $1 $2 \e[39m"
  else
    echo -e "$(date +%Y-%m-%d' '%H:%M:%S) $1 $2"
  fi
}