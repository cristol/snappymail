#!/bin/sh

REPSCRIPT=$(cd $(dirname $0); pwd)
source $REPSCRIPT/fonctions.sh

# Set PUID, PGID et DOMAIN
PUID=${PUID:-65534}
PGID=${PGID:-65534}
DOMAIN=${DOMAIN:-_}
TZ=${TZ:-Europe/Paris}

echo "PUID = $PUID"
echo "PGID = $PGID"
echo "DOMAIN = $DOMAIN"
echo "TZ = $TZ"

# Modification du TZ
CONTINENT=$(echo $TZ | awk -F/ '{print $1}')
CAPITALE=$(echo $TZ | awk -F/ '{print $2}')
cp /usr/share/zoneinfo/$CONTINENT/$CAPITALE /etc/localtime
if [ $? -ne 0 ]; then
  LOG ERROR "Le timezone $TZ semble invalide"
  exit 1
fi
echo $TZ > /etc/timezone
echo "Date actuelle avec le timezone $TZ : $(date)"

# Modification du PUID et PGID pour nobody
usermod -o -u $PUID nobody > /dev/null 2>&1
groupmod -o -g $PGID nobody > /dev/null 2>&1

if [ $DOMAIN == "_" ]; then
  LOG INFO "Activation du Vhost en site par defaut"
  sed -i "s/DOMAIN/$DOMAIN/g" ./confs/domain.conf
  cp ./confs/domain.conf /etc/nginx/http.d/default.conf
else
  LOG INFO "Activation du Vhost pour $DOMAIN"
  sed -i "s/DOMAIN/$DOMAIN/g" ./confs/domain.conf
  cp ./confs/domain.conf /etc/nginx/http.d/$DOMAIN.conf
  cp ./confs/default.conf /etc/nginx/http.d/default.conf
fi

# Generation de certificat
mkdir -p /etc/nginx/cert
cd /etc/nginx/cert

# Creation d'un certificat autosigne si non present.
if [[ ! -f fullchain.pem ]] || [[ ! -f privkey.pem ]]; then
  LOG WARN "Generation du certificat auto signé en cours"
  openssl req -new -newkey rsa:4096 -x509 -sha256 -days 3650 -nodes -out fullchain.pem -keyout privkey.pem \
  -subj "/C=FR/ST=Paris/L=Paris/O=Cristol_Snappymail/OU=Cristol&CO/CN=$DOMAIN" > /dev/null 2>&1
  if [ $? -ne 0 ]; then
    LOG ERROR "Generation du certificat auto signé impossible"
  else
    LOG INFO "Generation du certificat auto signé OK"
  fi
else
  LOG INFO "Certificat deja existant"
fi

LOG INFO "Deploiement de Snappymail"
php ${WWW_PATH}/index.php > /dev/null 2>&1

# Ajout du provider Orange et de son Alias
mkdir -p ${WWW_PATH}/data/_data_/_default_/domains/
if [ ! -f ${WWW_PATH}/data/_data_/_default_/domains/orange.fr.json ]; then
  cp $REPSCRIPT/../providers/orange.fr.json ${WWW_PATH}/data/_data_/_default_/domains/orange.fr.json
  echo 'orange.fr' > ${WWW_PATH}/data/_data_/_default_/domains/wanadoo.fr.alias
  if [ -f ${WWW_PATH}/data/_data_/_default_/domains/disabled ]; then
    sed 's/$/,orange.fr,wanadoo.fr/' -i ${WWW_PATH}/data/_data_/_default_/domains/disabled
  fi
  if [ -f ${WWW_PATH}/data/_data_/_default_/domains/orange.fr.ini ]; then
    LOG WARN "Suppression de l'ancienne version du domaine Orange.fr"
    rm ${WWW_PATH}/data/_data_/_default_/domains/orange.fr.ini
  fi
  for FILE in $(grep -r '"name": "\*"' ${WWW_PATH}/data/_data_/_default_/domains | awk -F: '{print $1}'); do
    rm $FILE
  done
fi

# Suppression de ce script
cd $OLDPWD
rm $0
