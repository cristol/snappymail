FROM alpine:3.21.3
LABEL authors="Cristol Bardou"

ENV WWW_PATH=/var/www/snappymail

RUN apk add --no-cache \
    shadow \
    tzdata \
    openssl \
    nginx \
    gnupg \
    php \
    php-common \
    php-curl \
    php-xml \
    php-fpm \
    php-json \
    php-mysqli \
    php-iconv \
    php-dom \
    php-pdo_sqlite \
    php-mbstring \
    php-fileinfo \
    php-imagick \
    php-zip \
    php-gd \
    php-sodium \
    php-intl \
    php-tidy \
    php-phar \
    wget \
    curl \
    unzip && \
    rm -rf /usr/lib/perl* /usr/share/perl*

RUN wget https://github.com/the-djmaze/snappymail/releases/download/v2.38.2/snappymail-2.38.2.zip && \
    unzip snappymail-2.38.2.zip -d $WWW_PATH && \
    rm snappymail-2.38.2.zip && \
    sed -i 's/user nginx/user nobody nobody/' /etc/nginx/nginx.conf && \
    sed -i 's/upload_max_filesize =.*/upload_max_filesize = 100M/g' /etc/php*/php.ini && \
    sed -i 's/post_max_size =.*/post_max_size = 100M/g' /etc/php*/php.ini

WORKDIR /app

ADD rootfs/ .

EXPOSE 80 443

VOLUME [ "${WWW_PATH}/data" ]

ENTRYPOINT ["sh", "./entrypoint.sh"]
