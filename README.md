# Snappymail Dockerfile

[![pipeline status](https://gitlab.com/cristol/snappymail/badges/master/pipeline.svg)](https://gitlab.com/cristol/snappymail/-/commits/master) 

> Création d'une image Snappymail sur alpine avec Nginx et php8

> [Registry Docker Hub](https://hub.docker.com/r/cristol/snappymail)

## Deploiement depuis docker hub

### Docker run

```bash
docker run -d --name Snappymail \
  -e PUID=1000 \
  -e PGID=1000 \
  -e DOMAIN=YourDomain \
  -e TZ=Europe/Paris \
  -v ~/.Snappymail_Data:/var/www/snappymail/data \
  -v YourCertificateFile:/etc/nginx/cert/fullchain.pem:ro \
  -v YourCertificateKeyFile:/etc/nginx/cert/privkey.pem:ro \
  -p 8080:80 \
  -p 8443:443 \
  cristol/snappymail:latest
```

### Docker compose

```yml
version: "3.8"
services:
  snappymail:
    image: cristol/snappymail:latest
    container_name: Snappymail
    restart: always
    environment:
      - PUID=1000
      - PGID=1000
      - DOMAIN=YourDomain
      - TZ=Europe/Paris
    volumes:
      - ~/.Snappymail_Data:/var/www/snappymail/data
      - YourCertificateFile:/etc/nginx/cert/fullchain.pem:ro
      - YourCertificateKeyFile:/etc/nginx/cert/privkey.pem:ro
    ports:
      - 8080:80
      - 8443:443
```

## Deploiment depuis le Dockerfile

### Création de l'image

```bash
docker build -t cristol/snappymail:latest .
```

---
Cristol Bardou